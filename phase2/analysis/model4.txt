Model 4:
Summary:

Call:
lm(formula = Sales ~ Advertising, data = data)

Residuals:
    Min      1Q  Median      3Q     Max 
-7.0403 -1.9253  0.0336  1.5577  7.4136 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)  6.49638    0.25400  25.577  < 2e-16 ***
Advertising  0.13056    0.02898   4.505 1.15e-05 ***
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Residual standard error: 2.556 on 193 degrees of freedom
Multiple R-squared:  0.09516,	Adjusted R-squared:  0.09047 
F-statistic:  20.3 on 1 and 193 DF,  p-value: 1.147e-05


Coefficients:
(Intercept) Advertising 
  6.4963781   0.1305622 

ANOVA:
Analysis of Variance Table

Response: Sales
             Df  Sum Sq Mean Sq F value    Pr(>F)    
Advertising   1  132.59 132.585  20.297 1.147e-05 ***
Residuals   193 1260.73   6.532                      
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1



	Shapiro-Wilk normality test

data:  residuals(model)
W = 0.99349, p-value = 0.5475


	studentized Breusch-Pagan test

data:  model
BP = 0.13581, df = 1, p-value = 0.7125

Points atypiques: 7 40 50 85 90 95 150 168 181 183 
Valeurs des points atypiques: 6.496378 7.410314 6.496378 7.410314 6.496378 6.496378 8.454812 7.410314 6.496378 6.496378 
                 2.5 %   97.5 %
(Intercept) 5.99541406 6.997342
Advertising 0.07340353 0.187721
