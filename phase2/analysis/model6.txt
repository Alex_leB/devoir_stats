Model 6:
Summary:

Call:
lm(formula = log(Sales) ~ Advertising, data = data)

Residuals:
    Min      1Q  Median      3Q     Max 
-3.5937 -0.2072  0.0999  0.2916  0.8715 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept) 1.761095   0.053191  33.109  < 2e-16 ***
Advertising 0.020485   0.006069   3.375 0.000891 ***
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Residual standard error: 0.5352 on 193 degrees of freedom
Multiple R-squared:  0.05574,	Adjusted R-squared:  0.05085 
F-statistic: 11.39 on 1 and 193 DF,  p-value: 0.000891


Coefficients:
(Intercept) Advertising 
 1.76109527  0.02048544 

ANOVA:
Analysis of Variance Table

Response: log(Sales)
             Df Sum Sq Mean Sq F value   Pr(>F)    
Advertising   1  3.264  3.2640  11.394 0.000891 ***
Residuals   193 55.290  0.2865                     
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1



	Shapiro-Wilk normality test

data:  residuals(model)
W = 0.74174, p-value < 2.2e-16


	studentized Breusch-Pagan test

data:  model
BP = 0.96787, df = 1, p-value = 0.3252

Points atypiques: 40 50 131 150 168 181 
Valeurs des points atypiques: 1.904493 1.761095 1.761095 2.068377 1.904493 1.761095 
                  2.5 %     97.5 %
(Intercept) 1.656184799 1.86600574
Advertising 0.008515424 0.03245546
