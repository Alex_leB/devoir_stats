Model 3:
Summary:

Call:
lm(formula = log(Sales) ~ Price, data = data)

Residuals:
    Min      1Q  Median      3Q     Max 
-3.4683 -0.1729  0.0690  0.2751  0.9051 

Coefficients:
             Estimate Std. Error t value Pr(>|t|)    
(Intercept)  3.127869   0.187708   16.66  < 2e-16 ***
Price       -0.010735   0.001593   -6.74 1.79e-10 ***
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Residual standard error: 0.4956 on 193 degrees of freedom
Multiple R-squared:  0.1905,	Adjusted R-squared:  0.1863 
F-statistic: 45.42 on 1 and 193 DF,  p-value: 1.785e-10


Coefficients:
(Intercept)       Price 
 3.12786863 -0.01073498 

ANOVA:
Analysis of Variance Table

Response: log(Sales)
           Df Sum Sq Mean Sq F value    Pr(>F)    
Price       1 11.156 11.1556  45.424 1.785e-10 ***
Residuals 193 47.398  0.2456                      
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1



	Shapiro-Wilk normality test

data:  residuals(model)
W = 0.7864, p-value = 1.354e-15


	studentized Breusch-Pagan test

data:  model
BP = 7.8679, df = 1, p-value = 0.005032

Points atypiques: 15 40 50 114 131 168 181 
Valeurs des points atypiques: 2.16172 1.421007 1.635706 1.828936 1.700116 1.077487 1.871876 
                  2.5 %       97.5 %
(Intercept)  2.75764645  3.498090821
Price       -0.01387649 -0.007593469
