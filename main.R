cat("Devoir MTH2302D - Hiver 2023")
cat("Étudiant: ")
cat(" - Louis-Philippe Daigle (2139028)")
cat(" - Alexis LeBlanc (2150199)")

source("charger.R")
data <- charger(2150199)

# Phase 1
source("phase1.R")

# Phase 2
source("phase2.R")
